# Profiling and optimizing Python code

## Resources

-   [Youtube - Profiling and optimizing your Python code | Python tricks](https://youtu.be/8qEnExGLZfY)
-   [Jupyter Notebook](https://osf.io/upav8/)

## Questions

### Do you need optimization?

-   If speed is not a problem, then there is no reason to optimize

### Which parts of your code should be optimized?

-   Use a profiler, such as cProfile
-   Usually, almost all execution time occurs within a small part of your code
-   Optimize that code, and leave the rest alone

### If you need even better performance

-   Redesign the code completely
