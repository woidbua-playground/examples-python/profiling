import cProfile
import io
import pstats
from random import shuffle


def profile(fnc):
    """A decorator that uses cProfile to profile a function"""

    def inner(*args, **kwargs):
        pr = cProfile.Profile()
        pr.enable()
        retval = fnc(*args, **kwargs)
        pr.disable()
        s = io.StringIO()
        sortby = 'cumulative'
        ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
        ps.print_stats()
        print(s.getvalue())
        return retval

    return inner


def read_movies(src):
    with open(src) as in_file:
        return in_file.readlines()


def write_movies(src, movies):
    with open(src, "w") as out_file:
        out_file.writelines(movies)


def shuffle_movies(src):
    movies = read_movies(src=src)
    shuffle(movies)
    write_movies(src=src, movies=movies)


@profile
def find_duplicate_movies_bad():
    movies = read_movies(src="movies.txt")
    duplicates = list()
    while movies:
        movie = movies.pop()
        if is_duplicate(movie, movies):
            duplicates.append(movie)
    return duplicates


def is_duplicate(needle, haystack):
    for movie in haystack:
        if needle.lower() == movie.lower():
            return True
    return False


@profile
def find_duplicate_movies_good():
    movies = read_movies(src="movies.txt")
    movies = [movie.lower() for movie in movies]
    movies.sort()
    duplicates = [movie1 for movie1, movie2 in zip(movies[:-1], movies[1:]) if movie1 == movie2]
    return duplicates


print(find_duplicate_movies_bad())
print(find_duplicate_movies_good())
